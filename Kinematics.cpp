/*
 * Kinematics.cpp
 *
 *  Created on: Feb 8 2017
 *      Author: Karthik Srivatsan
 *      adapted from Maxim Zavatsky's code
 */
#include <math.h>
#include <iostream>
using namespace std;

//Dimensions of the delta arm I'm using.
 const float e = 57.0;      // end effector
 const float f = 123.0;     // base
 const float re = 195.0;    //"forearm"
 const float rf = 120.0;    //"upper arm"
 const double pi = 3.14159265358979; //pi

// inverse kinematics
 // calculates and returns theta1
 //takes in "status" to make sure that the position is valid
 float delta_calcAngleYZ(float x0, float y0, float z0, int &status) {
	 double tan30 = tan(pi/6);
     float y1 = -0.5 * tan30 * f; // f/2 * tg 30
     y0 -= 0.5 * tan30 * e;    // shift center to edge
     // z = a + b*y
     float a = (x0*x0 + y0*y0 + z0*z0 +rf*rf - re*re - y1*y1)/(2*z0);
     float b = (y1-y0)/z0;
     // discriminant
     float d = -(a+b*y1)*(a+b*y1)+rf*(b*b*rf+rf);
     if (d < 0){
    	 status = -1; // non-existing point
    	 cout << "Error: point out of range";
     }
     float yj = (y1 - a*b - sqrt(d))/(b*b + 1); // choosing outer point
     float zj = a + b*yj;
     float theta = atan(-zj/(y1 - yj));
     if(yj>y1){
    	 theta = theta+pi; //removes the pesky ?: operator
     }
     if(theta < 0 || theta > pi/2){
    	 status = -1;
     }
     return theta;
 }

 // inverse kinematics: (x0, y0, z0) -> (theta1, theta2, theta3)
 // returned status: 0=OK, -1=non-existing position
 int delta_calcInverse(float x0, float y0, float z0,
		 float &theta1, float &theta2, float &theta3) {
	 double sin120 = sin(2*pi/3);
	 double cos120 = cos(2*pi/3);
     int status = 0;
     theta1 = delta_calcAngleYZ(x0, y0, z0, status)-(pi/2);
     if (status == 0){
    	 theta2 = delta_calcAngleYZ(x0*cos120 + y0*sin120, y0*cos120-x0*sin120, z0, status)-(pi/2);
    	 // rotate coords to +120 deg
     }
     if (status == 0){
    	 theta3 = delta_calcAngleYZ(x0*cos120 - y0*sin120, y0*cos120+x0*sin120, z0, status)-(pi/2);
    	 // rotate coords to -120 deg
     }
     return status;
 }
