/*
 * Kinematics_Test.cpp
 *
 *  Created on: Feb 17, 2017
 *      Author: Karthik
 */

#include "Kinematics.h"
#include <iostream>

using namespace std;

int main(){
	float x,y,z;
	float theta1,theta2,theta3;
	std::string str1;
	while(true){
		std::cout << "Input x:";
		std::cin >> str1;
		if(str1 != "q" && str1 != "quit"){
			x = std::stoi(str1);
		}
		else{
			break;
		}
		std::cout << "Input y:";
		std::cin >> str1;
		if(str1 != "q" && str1 != "quit"){
			y = std::stoi(str1);
		}
		else{
			break;
		}
		std::cout << "Input z:";
		std::cin >> str1;
		if(str1 != "q" && str1 != "quit"){
			z = std::stoi(str1);
		}
		else{
			break;
		}
		int status = delta_calcInverse(x,y,z,theta1,theta2,theta3);
		cout << theta1 << " " << theta2 << " " << theta3 << endl;
		cout << "Status: " << status << endl;
	}
}


