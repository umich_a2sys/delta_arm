/*
 * Delta_Arm_Test.cpp
 *
 *  Created on: Mar 12, 2017
 *      Author: Karthik
 *      Adapted from UM A2Sys bucket drop code.
 */

#include "Kinematics.h"
#include "servo.h"
#include "bbblib/bbb.h"

int setManipulatorPosition(float x, float y, float z) {
	float theta1 = 0;
	float theta2 = 0;
	float theta3 = 0;

  // Check (x,y,z) is valid
  if(delta_calcInverse(x, y, z, theta1, theta2, theta3) == 0){
	  std::cout << "theta1: " << theta1;
	  std::cout << " theta2: " << theta2;
	  std::cout << " theta3: " << theta3 << std::endl;
	  setAnglesTop(theta1, theta2, theta3);
    return 1;
  }
  else{
	  std::cout << "Error: position out of range" << std::endl;
  }
  return 0;
}
/**
  initDelta()
  -- initializes delta arm power control
  -- turns on delta arm power
  -- initializes servos, setting all servos to STOW position
*/
int initDelta(){
  bbb_initGPIO(45, gpio_output);  // Delta Power
  bbb_writeGPIO(45, 1);  // turn on delta power
  initServos();  // set delta to stow with claw open
  return 0;
}

int powerDelta(int power){
  bbb_writeGPIO(45, power);
  return 0;
}

int main(){
	initDelta();
	setAnglesTop(0,0,0);
	float x,y,z;
	std::string str1;
	while(true){
		std::cout << "Input x:";
		std::cin >> str1;
		if(str1 != "q" && str1 != "quit"){
			x = std::stoi(str1);
		}
		else{
			break;
		}
		std::cout << "Input y:";
		std::cin >> str1;
		if(str1 != "q" && str1 != "quit"){
			y = std::stoi(str1);
		}
		else{
			break;
		}
		std::cout << "Input z:";
		std::cin >> str1;
		if(str1 != "q" && str1 != "quit"){
			z = std::stoi(str1);
		}
		else{
			break;
		}
		setManipulatorPosition(x,y,z);
	}
	setAnglesTop(0,0,0);
}
