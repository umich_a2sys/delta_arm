all: Kinematics_Test

Kinematics_Test: Kinematics_Test.cpp Kinematics.cpp
	g++ -Wall -Werror -pedantic -O1 --std=c++11 Kinematics_Test.cpp Kinematics.cpp -o Kinematics_Test.exe
	
clean:
	rm -vf *~ *.exe