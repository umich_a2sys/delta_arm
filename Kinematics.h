/*
 * Kinematics.h
 *
 *  Created on: Feb 17, 2017
 *      Author: Karthik
 */

#ifndef KINEMATICS_H_
#define KINEMATICS_H_

float delta_calcAngleYZ(float x0, float y0, float z0, int &status);
int delta_calcInverse(float x0, float y0, float z0,float &theta1, float &theta2, float &theta3);

#endif /* KINEMATICS_H_ */
