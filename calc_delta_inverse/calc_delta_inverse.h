/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 * File: calc_delta_inverse.h
 *
 * MATLAB Coder version            : 3.0
 * C/C++ source code generated on  : 08-Nov-2016 17:26:02
 */

#ifndef __CALC_DELTA_INVERSE_H__
#define __CALC_DELTA_INVERSE_H__

/* Include Files */
#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "calc_delta_inverse_types.h"

/* Function Declarations */
extern void calc_delta_inverse(double x0, double b_y0, double z0, double *theta1,
  double *theta2, double *theta3);

#endif

/*
 * File trailer for calc_delta_inverse.h
 *
 * [EOF]
 */
