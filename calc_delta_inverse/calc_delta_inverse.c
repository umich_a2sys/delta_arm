/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 * File: calc_delta_inverse.c
 *
 * MATLAB Coder version            : 3.0
 * C/C++ source code generated on  : 08-Nov-2016 17:26:02
 */

/* Include Files */
#include "rt_nonfinite.h"
#include "calc_delta_inverse.h"

/* Function Declarations */
static void delta_angle(double x0, double b_y0, double z0, double *status,
  double *theta);

/* Function Definitions */

/*
 * Helper function for calculating angles on a Delta Arm.
 * Outputs angles in degrees.
 * Arguments    : double x0
 *                double b_y0
 *                double z0
 *                double *status
 *                double *theta
 * Return Type  : void
 */
static void delta_angle(double x0, double b_y0, double z0, double *status,
  double *theta)
{
  double a;
  double b;
  double d;
  double yj;

  /* end effector */
  /* base */
  *theta = 0.0;
  b_y0 -= 33.197640478403478;

  /* shift center to edge */
  a = (((((x0 * x0 + b_y0 * b_y0) + z0 * z0) + 12544.0) - 53824.0) -
       17426.940833333334) / (2.0 * z0);
  b = (-132.01113905020793 - b_y0) / z0;

  /* discriminant */
  d = -(a + b * -132.01113905020793) * (a + b * -132.01113905020793) + 112.0 *
    (b * b * 112.0 + 112.0);
  if (d < 0.0) {
    *status = -1.0;
  } else {
    yj = ((-132.01113905020793 - a * b) - sqrt(d)) / (b * b + 1.0);

    /* choosing outer point */
    *theta = 180.0 * atan(-(a + b * yj) / (-132.01113905020793 - yj)) /
      3.1415926535897931;
    if (yj > -132.01113905020793) {
      *theta += 180.0;
    }

    *status = 0.0;
  }
}

/*
 * Calculates the inverse kinematics of a Delta Arm.
 * Arguments    : double x0
 *                double b_y0
 *                double z0
 *                double *theta1
 *                double *theta2
 *                double *theta3
 * Return Type  : void
 */
void calc_delta_inverse(double x0, double b_y0, double z0, double *theta1,
  double *theta2, double *theta3)
{
  double status;
  *theta2 = 0.0;
  *theta3 = 0.0;
  delta_angle(x0, b_y0, z0, &status, theta1);
  if (status != 0.0) {
  } else {
    delta_angle(x0 * 0.50000000000000011 + b_y0 * 0.8660254037844386, b_y0 *
                0.50000000000000011 - x0 * 0.8660254037844386, z0, &status,
                theta2);
    if (status != 0.0) {
    } else {
      delta_angle(x0 * 0.500000000000000 - b_y0 * 0.8660254037844386, b_y0 *
                  0.500000000000000 + x0 * 0.8660254037844386, z0, &status,
                  theta3);
    }
  }
}

/*
 * File trailer for calc_delta_inverse.c
 *
 * [EOF]
 */
