/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 * File: _coder_calc_delta_inverse_mex.h
 *
 * MATLAB Coder version            : 3.0
 * C/C++ source code generated on  : 08-Nov-2016 17:26:02
 */

#ifndef ___CODER_CALC_DELTA_INVERSE_MEX_H__
#define ___CODER_CALC_DELTA_INVERSE_MEX_H__

/* Include Files */
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include "tmwtypes.h"
#include "mex.h"
#include "emlrt.h"
#include "_coder_calc_delta_inverse_api.h"

/* Function Declarations */
extern void mexFunction(int32_T nlhs, mxArray *plhs[], int32_T nrhs, const
  mxArray *prhs[]);
extern emlrtCTX mexFunctionCreateRootTLS(void);

#endif

/*
 * File trailer for _coder_calc_delta_inverse_mex.h
 *
 * [EOF]
 */
