/* 
 * Academic License - for use in teaching, academic research, and meeting 
 * course requirements at degree granting institutions only.  Not for 
 * government, commercial, or other organizational use. 
 * File: _coder_calc_delta_inverse_info.h 
 *  
 * MATLAB Coder version            : 3.0 
 * C/C++ source code generated on  : 08-Nov-2016 17:26:02 
 */

#ifndef ___CODER_CALC_DELTA_INVERSE_INFO_H__
#define ___CODER_CALC_DELTA_INVERSE_INFO_H__
/* Include Files */ 
#include "tmwtypes.h"
#include "mex.h"
#include "emlrt.h"


/* Function Declarations */ 
extern const mxArray *emlrtMexFcnResolvedFunctionsInfo(void);
MEXFUNCTION_LINKAGE mxArray *emlrtMexFcnProperties(void);

#endif
/* 
 * File trailer for _coder_calc_delta_inverse_info.h 
 *  
 * [EOF] 
 */
