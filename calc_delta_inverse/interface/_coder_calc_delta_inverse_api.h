/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 * File: _coder_calc_delta_inverse_api.h
 *
 * MATLAB Coder version            : 3.0
 * C/C++ source code generated on  : 08-Nov-2016 17:26:02
 */

#ifndef ___CODER_CALC_DELTA_INVERSE_API_H__
#define ___CODER_CALC_DELTA_INVERSE_API_H__

/* Include Files */
#include "tmwtypes.h"
#include "mex.h"
#include "emlrt.h"
#include <stddef.h>
#include <stdlib.h>
#include "_coder_calc_delta_inverse_api.h"

/* Variable Declarations */
extern emlrtCTX emlrtRootTLSGlobal;
extern emlrtContext emlrtContextGlobal;

/* Function Declarations */
extern void calc_delta_inverse(real_T x0, real_T b_y0, real_T z0, real_T *theta1,
  real_T *theta2, real_T *theta3);
extern void calc_delta_inverse_api(const mxArray * const prhs[3], const mxArray *
  plhs[3]);
extern void calc_delta_inverse_atexit(void);
extern void calc_delta_inverse_initialize(void);
extern void calc_delta_inverse_terminate(void);
extern void calc_delta_inverse_xil_terminate(void);

#endif

/*
 * File trailer for _coder_calc_delta_inverse_api.h
 *
 * [EOF]
 */
