/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 * File: calc_delta_inverse_initialize.c
 *
 * MATLAB Coder version            : 3.0
 * C/C++ source code generated on  : 08-Nov-2016 17:26:02
 */

/* Include Files */
#include "rt_nonfinite.h"
#include "calc_delta_inverse.h"
#include "calc_delta_inverse_initialize.h"

/* Function Definitions */

/*
 * Arguments    : void
 * Return Type  : void
 */
void calc_delta_inverse_initialize(void)
{
  rt_InitInfAndNaN(8U);
}

/*
 * File trailer for calc_delta_inverse_initialize.c
 *
 * [EOF]
 */
