/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 * File: calc_delta_inverse_types.h
 *
 * MATLAB Coder version            : 3.0
 * C/C++ source code generated on  : 08-Nov-2016 17:26:02
 */

#ifndef __CALC_DELTA_INVERSE_TYPES_H__
#define __CALC_DELTA_INVERSE_TYPES_H__

/* Include Files */
#include "rtwtypes.h"
#endif

/*
 * File trailer for calc_delta_inverse_types.h
 *
 * [EOF]
 */
